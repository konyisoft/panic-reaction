﻿namespace Konyisoft.Panic
{
	public class Constants 
	{
		public class Tags
		{
			public const string Player = "Player";
		}

		public class Layers
		{
			public const int Level = 8;
			public const int Player = 9;
		}

		public class LayerMasks
		{
			public const int Level = 1 << Layers.Level;
			public const int Player = 1 << Layers.Player;
		}
		
		public class ObjectNames
		{
			public const string Level = "Level";
		}
		
		public class Resources
		{
			public const string Prefabs  = "Prefabs";

			public const string Explosion = Prefabs + "/Explosion";
		}

		public class Texts
		{
			public const string Score = "<color=#d3d3d3ff>SCORE</color> {0}";
			public const string Timer = "<color=#d3d3d3ff>TIME</color> {0:00}:{1:00}";
		}
		
		public class GameStartValues
		{
			public const int Score = 0;
			public const int Health = 100;
			public const int Lives = 3;
		}
	}
}

