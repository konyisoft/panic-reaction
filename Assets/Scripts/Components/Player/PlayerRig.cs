﻿using UnityEngine;
using System.Collections.Generic;

namespace Konyisoft.Panic
{
	public class PlayerRig : GameBehaviour
	{
		#region Properties

		public PlayerMoveDirection Direction
		{
			get { return GetMoveDirection(); }
		}

		public float MoveSpeed
		{
			get { return Rigidbody.velocity.magnitude; }
		}

		public float SqrMoveSpeed  // faster
		{
			get { return Rigidbody.velocity.sqrMagnitude; }
		}

		public bool IsColliding
		{
			get; private set;
		}

		public PlayerJumpState JumpState
		{
			get { return jumpState; }
		}

		private Rigidbody Rigidbody
		{
			get; set;
		}

		#endregion

		#region Fields

		public float rotateSpeed = 1f;
		public float rotateDamping = 1f;
		public float jumpForce = 1f;
		public float gravity = -9.81f;
		public float cameraFollowSpeed = 5f;
		public float maxSpeed = 50f;

		PlayerJumpState jumpState = PlayerJumpState.None;
		float deltaZRotation;
		ContactPoint[] contactPoints = new ContactPoint[0];
		Quaternion currentRotation;
		Transform cameraTransform;
		int prevContactCount;

		#endregion

		#region Delegates

		public delegate void OnRigEvent();
		public OnRigEvent onBump;
		public OnRigEvent onJump;

		#endregion

		#region Mono methods

		void Awake()
		{
			Rigidbody = GetComponent<Rigidbody>();
			Physics.gravity = Vector3.up * gravity;
			cameraTransform = DisplayManager.Instance.playerCamera.transform;
		}

		void Update()
		{
			// Camera position & rotation follow
			if (cameraTransform != null)
			{
				// Position
				Vector3 position = transform.position;
				position.z = cameraTransform.position.z;
				DisplayManager.Instance.SetPlayerCameraPosition(
					Vector3.Lerp(cameraTransform.position, position, Time.deltaTime * cameraFollowSpeed)
				);

				// Rotation
				DisplayManager.Instance.SetPlayerCameraRotation(
					Quaternion.Lerp(cameraTransform.rotation, transform.rotation, Time.deltaTime * cameraFollowSpeed)
				);
			}
		}

		void FixedUpdate()
		{
			// Rotating the body & gravity.
			if (Mathf.Abs(deltaZRotation) > 0)
			{
				Rigidbody.MoveRotation(Rigidbody.rotation * Quaternion.Euler(0, 0, deltaZRotation));
				Physics.gravity = transform.up * gravity;
			}

			// Constrainig move speed
			if (SqrMoveSpeed > maxSpeed * maxSpeed)
			{
				Rigidbody.velocity = Rigidbody.velocity.normalized * maxSpeed;
			}

			// Handle jumping
			if (IsColliding && jumpState == PlayerJumpState.Jumping)
			{
				Vector3 jumpDirection = GetJumpDirection();
				if (jumpDirection != Vector3.zero)
				{
					// It is important to zero the current Y velocity before adding the jump force
					Rigidbody.velocity = VectorUtils.VectorXZ(Rigidbody.velocity);
					// Add a jump force
					Rigidbody.AddForce(jumpDirection * jumpForce, ForceMode.VelocityChange);
					// Call event handlers
					if (onJump != null)
					{
						onJump();
					}
				}
				jumpState = PlayerJumpState.Jumped;
			}

			// End of jump
			if (jumpState == PlayerJumpState.Jumped)
			{
				jumpState = PlayerJumpState.None;
			}

 			// Handle bump event (for playing audio)
			if (jumpState != PlayerJumpState.None || onBump == null)
			{
				return;
			}

			int contactCount = contactPoints.Length;
			bool hasImpact = false;

			for (int i = 0; i < contactCount; i++)
			{
				float impactForce = Vector3.Dot(Rigidbody.velocity, contactPoints[i].normal);
				if (impactForce > 1.5f) // magic constants
				{
					hasImpact = true;
					break;
				}
			}

			if (hasImpact && contactCount > prevContactCount)
			{
				 onBump();
			}

			prevContactCount = contactCount;
		}

		#endregion

		#region Public methods

		public void RotateLeft()
		{
			deltaZRotation = -Time.deltaTime * rotateSpeed;
		}

		public void RotateRight()
		{
			deltaZRotation = Time.deltaTime * rotateSpeed;
		}

		public void CalmRotation()
		{
			// Let it be lerped to 0 if no keypress
			if (Mathf.Abs(deltaZRotation) > 0)
			{
				deltaZRotation = Mathf.Lerp(deltaZRotation, 0, Time.deltaTime * rotateDamping);
			}
		}

		public void SetJumpState(PlayerJumpState jumpState)
		{
			this.jumpState = jumpState;
		}

		#endregion

		#region Private methods		

		PlayerMoveDirection GetMoveDirection()
		{
			float angle = Vector3.Angle(Rigidbody.velocity, transform.right);
			if (angle > 90f)
			{
				return PlayerMoveDirection.Left;
			}
			else if (angle < 90f)
			{
				return PlayerMoveDirection.Right;
			}
			else
			{
				return PlayerMoveDirection.None;
			}
		}

		Vector3 GetJumpDirection()
		{
			return IsColliding ?
				(transform.position - GetContactPointsCenter()).normalized :
				Vector3.zero;
		}

		Vector3 GetContactPointsCenter()
		{
			Vector3 v = Vector3.zero;
			if (contactPoints.Length > 0)
			{
				for (int i = 0; i < contactPoints.Length; i++)
				{
					v += contactPoints[i].point;
				}
				v /= contactPoints.Length;
			}
			return v;
		}

		#endregion

		#region Event handlers
		void OnCollisionEnter(Collision collisionInfo)
		{
			if (collisionInfo.gameObject.layer == Constants.Layers.Level)
			{
				IsColliding = true;
				contactPoints = collisionInfo.contacts;
			}
		}

		void OnCollisionStay(Collision collisionInfo)
		{
			if (collisionInfo.gameObject.layer == Constants.Layers.Level)
			{
				IsColliding = true;
				contactPoints = collisionInfo.contacts;
			}
		}

		void OnCollisionExit(Collision collisionInfo)
		{
			if (collisionInfo.gameObject.layer == Constants.Layers.Level)
			{
				IsColliding = false;
				prevContactCount = 0;
				contactPoints = new ContactPoint[0];
			}
		}

		#endregion
	}
}
