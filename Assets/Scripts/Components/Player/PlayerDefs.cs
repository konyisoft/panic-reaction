namespace Konyisoft.Panic
{
    #region Enumerations

    public enum PlayerMoveDirection
    {
        None,
        Left,
        Right,
    }

    public enum PlayerJumpState
    {
        None,
        Prepared,
        Jumping,
        Jumped
    }

    public enum PlayerState
    {
        Stopped,
        Playing,
        Dead
    }

    #endregion
}