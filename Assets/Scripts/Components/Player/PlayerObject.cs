﻿using UnityEngine;
using System.Collections;

namespace Konyisoft.Panic
{
	public class PlayerObject : GameBehaviour
	{
		#region Fields

		public float speedMultiplier = 1.5f;
		public float damping = 1f;

		float desiredZRotation;
		
		#endregion

		#region Mono methods

		void Update()
		{
			PlayerRig playerRig = Player.Instance.PlayerRig; // shortcut

			if (playerRig != null)
			{
				transform.position = playerRig.transform.position;
				
				if (playerRig.IsColliding)
				{
					switch (playerRig.Direction)
					{
						case PlayerMoveDirection.None :
							desiredZRotation = 0;
							break;
						case PlayerMoveDirection.Left :
							desiredZRotation = playerRig.MoveSpeed * speedMultiplier;
							break;
						case PlayerMoveDirection.Right :
							desiredZRotation = -playerRig.MoveSpeed * speedMultiplier;
							break;
					}
				}
				else
				{
					desiredZRotation = Mathf.Lerp(desiredZRotation, 0, Time.deltaTime * damping);
				}
				
				// Do the rotation
				if (desiredZRotation != 0)
				{
					transform.Rotate(0, 0, desiredZRotation);
				}
			}
		}
		
		#endregion
	}
}
