﻿using UnityEngine;

namespace Konyisoft.Panic
{
	public class Player : Singleton<Player>
	{
		#region Fields
		
		int fingerId = -1;
		bool jumpButtonDown = false;
		bool leftButtonDown = false;
		bool rightButtonDown = false;
		Vector3 previousPosition;

		#endregion

		#region Properties

		public PlayerRig PlayerRig
		{
			get; private set;
		}
		
		public PlayerObject PlayerObject
		{
			get; private set;
		}

		public PlayerState State
		{
			get; private set;
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			PlayerRig = GetComponentInChildren<PlayerRig>();
			if (PlayerRig == null)
			{
				Debug.LogError("No PlayerController attached to player.");
				return;
			}
			
			PlayerObject = GetComponentInChildren<PlayerObject>();
			if (PlayerObject == null)
			{
				Debug.LogError("No PlayerObject attached to player.");
				return;
			}

			PlayerRig.onBump += OnBump;
			PlayerRig.onJump += OnJump;
		}

		void Update()
		{
			// No rig
			if (PlayerRig == null)
				return;
			
			// Not in playing state?
			if (State != PlayerState.Playing)
			{
				PlayerRig.CalmRotation();
				return;
			}

			// Desktop
			#if UNITY_EDITOR || UNITY_WEBGL || UNITY_STANDALONE
				KeyboardInput();
			#endif
			
			// Mobile
			#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
				switch (Globals.Options.MobileInput) 
				{
					case MobileInputMode.Touch :
						TouchInput();
						break;
					case MobileInputMode.Buttons :
						// Does nothing, waits for left or right button press
						break;
				}
			#endif 

			// Common
			HandleJumpButtonPress();
			HandleDirectionButtonPress();

			// Moving background, calculate position delta
			UIManager.Instance.uiBackground.Move(PlayerRig.transform.position - previousPosition);
			previousPosition = PlayerRig.transform.position;
		}
		
		#endregion

		#region Public methods

		public void Activate()
		{
			gameObject.SetActive(true);
		}
		
		public void Deactivate()
		{
			gameObject.SetActive(false);
		}

		public void SetState(PlayerState state)
		{
			State = state;
		}
		
		#endregion

		#region Private methods

		void KeyboardInput()
		{
			if (!InputManager.Instance.IsMouseOverUI())
			{
				// Rotating
				if (Input.GetButton("Left"))
				{
					PlayerRig.RotateLeft();
				}
				else if (Input.GetButton("Right"))
				{
					PlayerRig.RotateRight();
				}
				else
				{
					PlayerRig.CalmRotation();
				}
				
				// Jump key down
				if (PlayerRig.JumpState == PlayerJumpState.None && Input.GetButtonDown("Jump"))
				{
					// Prepare for jumping
					PlayerRig.SetJumpState(PlayerJumpState.Prepared);
				}			
				
				// Key pressed continuously
				if (PlayerRig.JumpState == PlayerJumpState.Prepared && Input.GetButton("Jump"))
				{
					// Do a jump: will be handled in FixedUpdate
					PlayerRig.SetJumpState(PlayerJumpState.Jumping);
				}
				
				// Key up
				if (PlayerRig.JumpState != PlayerJumpState.None && Input.GetButtonUp("Jump"))
				{
					// Cancel jump
					PlayerRig.SetJumpState(PlayerJumpState.None);
				}
			}
		}

		void TouchInput()
		{
			if (Input.touchCount > 0)
			{
				// Looking for a began touch
				if (InputManager.Instance.HasBeganTouch())
				{
					Touch? touch = InputManager.Instance.GetLastBeganTouch();
					if (touch.HasValue && !InputManager.Instance.IsTouchOverUI(touch.Value.fingerId))
					{
						fingerId = touch.Value.fingerId;
					}
				}
				
				if (fingerId > -1 && !InputManager.Instance.IsTouchOverUI(fingerId))
				{
					Touch? touch = InputManager.Instance.GetTouchByFingerId(fingerId);
					if (touch.HasValue)
					{
						switch (touch.Value.phase)
						{
							// Handle left and right rotation
							case TouchPhase.Moved :
							case TouchPhase.Stationary :
								Rect leftRect = new Rect(0, 0, Screen.width * 0.5f, Screen.height);
								if (leftRect.Contains(touch.Value.position))
								{
									PlayerRig.RotateLeft();
								}
								else
								{
									PlayerRig.RotateRight();
								}
								break;
							
							// Drop current touch and wait for next to begin
							case TouchPhase.Ended :
							case TouchPhase.Canceled :
								fingerId = -1;
								break;
						}
					}
					else
					{
						fingerId = -1;
						PlayerRig.CalmRotation();
					}
				}
				else
				{
					PlayerRig.CalmRotation();
				}
			}
			else
			{
				fingerId = -1;
				PlayerRig.CalmRotation();
			}
		}
		
		void HandleJumpButtonPress()
		{
			if (jumpButtonDown && PlayerRig.JumpState == PlayerJumpState.Prepared && PlayerRig.IsColliding)
			{
				// Do a jump: will be handled in FixedUpdate
				PlayerRig.SetJumpState(PlayerJumpState.Jumping);
			}
		}

		void HandleDirectionButtonPress()
		{
			if (leftButtonDown)
			{
				PlayerRig.RotateLeft();
			}
			else if (rightButtonDown)
			{
				PlayerRig.RotateRight();
			}
			else
			{
				PlayerRig.CalmRotation();
			}
		}

		#endregion

		#region Event handlers
		
		public void OnJumpButtonDown()
		{
			if (PlayerRig.JumpState == PlayerJumpState.None)
			{
				PlayerRig.SetJumpState(PlayerJumpState.Prepared);
			}
			jumpButtonDown = true;										
		}
				
		public void OnJumpButtonUp()
		{
			if (PlayerRig.JumpState != PlayerJumpState.None)
			{
				PlayerRig.SetJumpState( PlayerJumpState.None);
			}
			jumpButtonDown = false;			
		}

		public void OnLeftButtonDown()
		{
			leftButtonDown = true;
			rightButtonDown = false; // cancel the other
		}
				
		public void OnLeftButtonUp()
		{
			leftButtonDown = false;			
		}

		public void OnRightButtonDown()
		{
			rightButtonDown = true;
			leftButtonDown = false; // cancel the other
		}
				
		public void OnRightButtonUp()
		{
			rightButtonDown = false;			
		}

		void OnBump()
		{
			AudioManager.AudioData audioData = AudioManager.Instance.GetAudioData("Bump");
			if (!AudioManager.Instance.IsPlaying(audioData) || AudioManager.Instance.GetTime(audioData) > 1f)
			{
				AudioManager.Instance.PlaySound(audioData, 0);
			}
		}

		void OnJump()
		{
			AudioManager.Instance.PlaySound("Jump");
		}	

		#endregion
	}
}
