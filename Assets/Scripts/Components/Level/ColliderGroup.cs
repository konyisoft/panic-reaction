﻿using UnityEngine;

namespace Konyisoft.Panic
{
	public class ColliderGroup : GameBehaviour
	{
		#region Fields

		public bool generateAtStart = true;

		#endregion

		#region Mono methods

		void Start()
		{
			if (generateAtStart)
			{
				UpdateCollider();
			}
		}

		#endregion
		
		#region Public methods
		
		public void UpdateCollider()
		{
			GenerateCollider();
		}

		#endregion

		#region Private methods

		void GenerateCollider()
		{
			Mesh mesh = MeshUtils.CombineMeshes(transform);  // can be null
			// Get (or add) collider component
			MeshCollider meshCollider = GetComponent<MeshCollider>();
			if (meshCollider == null)
				meshCollider = gameObject.AddComponent<MeshCollider>();
			meshCollider.sharedMesh = mesh;
		}
		
		#endregion
	}
}
