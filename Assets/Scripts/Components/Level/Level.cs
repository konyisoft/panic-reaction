﻿using UnityEngine;

namespace Konyisoft.Panic
{
    public class Level : GameBehaviour
	{
		#region Fields

		public int time = 60;
		public string description = string.Empty;
		public Sprite background;
		public Light mainLight;
		public Transform startPoint;

		Transform origLightParent;

		#endregion

		#region Properties

		#endregion

		#region Mono methods

		void Awake()
		{
			if (mainLight != null)
			{
				origLightParent = mainLight.transform.parent;
			}
		}

		#endregion

		#region Public methods

		public void SetLightParent(Transform parent)
		{
			if (mainLight != null)
			{
				mainLight.transform.SetParent(parent);
			}
		}

		public void ResetLightParent()
		{
			if (mainLight != null)
			{
				mainLight.transform.SetParent(origLightParent);
			}
		}

		#endregion
	}
}
