﻿using System.Collections;
using UnityEngine;

namespace Konyisoft.Panic
{
	public class Destroyable : LevelObject
	{
		#region Fields
		
		public int score = 10;
		public string audioId;

		bool isDestroying;
		
		#endregion

		#region Mono methods

		void OnDestroy()
		{
			StopAllCoroutines();
		}

		#endregion
		
		#region Protected methods
		
		protected override void TouchBegin()
		{
			if (isDestroying)
				return;

			if (!string.IsNullOrEmpty(audioId))
			{
				AudioManager.Instance.PlaySound(audioId);
			}
			DisplayManager.Instance.ShakePlayerCamera();
			isDestroying = true;
			StartCoroutine(CO_Destroy());
		}
		
		#endregion

		#region Private methods

		IEnumerator CO_Destroy()
		{
			// Wait some time to bounce back
			yield return new WaitForSeconds(0.05f);
			SetActive(false);
			GameManager.Instance.AddScore(score);
			UpdateColliderGroup();
			Destroy(gameObject);
		}

		#endregion
	}
}
