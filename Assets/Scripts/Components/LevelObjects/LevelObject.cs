﻿using UnityEngine;

namespace Konyisoft.Panic
{
	public class LevelObject : GameBehaviour
	{
		#region Protected methods
		
		protected virtual void TouchBegin()
		{
		}

		protected virtual void Touching()
		{
		}

		protected virtual void TouchEnd()
		{
		}

		protected void SetActive(bool active)
		{
			gameObject.SetActive(active);
		}

		protected void SetColliderEnabled(bool enabled)
		{
			Collider collider = GetComponent<Collider>();
			if (collider != null)
			{
				collider.enabled = enabled;
			}
		}

		protected void UpdateColliderGroup()
		{
			ColliderGroup colliderGroup = GetComponentInParent<ColliderGroup>();
			if (colliderGroup != null)
			{
				colliderGroup.UpdateCollider();
			}
		}

		#endregion
		
		#region Event handlers
		
		void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.CompareTag(Constants.Tags.Player) &&
				Player.Instance.State == PlayerState.Playing)
			{
				TouchBegin();
			}
		}

		void OnTriggerStay(Collider other)
		{
			if (other.gameObject.CompareTag(Constants.Tags.Player) &&
				Player.Instance.State == PlayerState.Playing)
			{
				Touching();
			}
		}

		void OnTriggerExit(Collider other)
		{
			if (other.gameObject.CompareTag(Constants.Tags.Player) &&
				Player.Instance.State == PlayerState.Playing)
			{
				TouchEnd();
			}
		}

		#endregion
	}
}
