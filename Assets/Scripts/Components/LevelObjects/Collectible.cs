﻿using UnityEngine;
using System.Collections;

namespace Konyisoft.Panic
{
	public class Collectible : LevelObject
	{
		#region Fields
	 
		public int score = 100;
		public string audioId;
	
		#endregion
		
		#region Protected methods

		protected override void TouchBegin()
		{
			SetColliderEnabled(false);
			if (!string.IsNullOrEmpty(audioId))
			{
				AudioManager.Instance.PlaySound(audioId);
			}
			GameManager.Instance.AddScore(score);
			Destroy(gameObject);
		}
		
		#endregion
	}
}
