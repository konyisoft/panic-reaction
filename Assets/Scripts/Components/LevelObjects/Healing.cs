﻿using UnityEngine;

namespace Konyisoft.Panic
{
	public class Healing : LevelObject
	{
		#region Fields

		public string audioId;		

		#endregion
		
		#region Protected methods
		
		protected override void TouchBegin()
		{
			SetActive(false);
			if (!string.IsNullOrEmpty(audioId))
			{
				AudioManager.Instance.PlaySound(audioId);
			}
			GameManager.Instance.SetHealth(Constants.GameStartValues.Health); // To the maximum
			Destroy(gameObject);
		}
		
		#endregion
	}
}
