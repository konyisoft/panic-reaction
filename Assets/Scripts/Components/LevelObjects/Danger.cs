﻿using UnityEngine;

namespace Konyisoft.Panic
{
	public class Danger : LevelObject
	{
		#region Fields

		public int healthDecreaseValue = 5;
		public float repeatRate = 1f;
		public string audioId;

		static float lastDecreaseTime;  // Static! Shared between danger objects
		static bool isDecreasingHealth; // Also

		#endregion

		#region Constants

		string DecreaseHealthMethod = "DecreaseHealth";

		#endregion
		
		#region Mono methods

		void OnDestroy()
		{
			CancelDecreaseHealth();
		}

		#endregion
		
		#region Protected methods
		
		protected override void Touching()
		{
			// Don't invoke Decrease method when another object is invoking that
			if (!isDecreasingHealth)
			{
				float elapsedTime = Time.time - lastDecreaseTime;
				float invokeDelay = elapsedTime >= repeatRate ? 0 : repeatRate - elapsedTime;
				isDecreasingHealth = true;
				InvokeRepeating(DecreaseHealthMethod, invokeDelay, repeatRate);
			}
		}

		protected override void TouchEnd()
		{
			CancelDecreaseHealth();
		}
		
		#endregion

		#region Private methods

		void DecreaseHealth()
		{
			if (Player.Instance.State == PlayerState.Playing)
			{
				GameManager.Instance.DecreaseHealth(healthDecreaseValue);
				AudioManager.AudioData audioData = AudioManager.Instance.GetAudioData(audioId);
				if (!AudioManager.Instance.IsPlaying(audioData))
				{
					AudioManager.Instance.PlaySound(audioData);
				}
				lastDecreaseTime = Time.time;
				isDecreasingHealth = true;
			}
			else
			{
				CancelDecreaseHealth();
			}
		}

		void CancelDecreaseHealth()
		{
			if (IsInvoking(DecreaseHealthMethod))
			{
				CancelInvoke(DecreaseHealthMethod);
			}
			isDecreasingHealth = false;
		}

		#endregion
	}
}
