using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Panic
{
	[RequireComponent(typeof(Image))]
	public class UIBackground : GameBehaviour
	{
		#region Fields

		public float speed = 5f;

		RectTransform rectTransform;
		Vector2 origOffsetMin;
		Vector2 origOffsetMax;

		#endregion

		#region Mono methods

		void Awake()
		{
			rectTransform = transform as RectTransform;
			origOffsetMin = rectTransform.offsetMin;
			origOffsetMax = rectTransform.offsetMax;
		}

		#endregion

		#region Public methods

		public void ResetPosition()
		{
			rectTransform.offsetMin = origOffsetMin;
			rectTransform.offsetMax = origOffsetMax;
		}

		public void Move(Vector2 delta)
		{
			Vector2 minOffset = rectTransform.offsetMin;
			Vector2 maxOffset = rectTransform.offsetMax;
			float clampX = Mathf.Abs(origOffsetMin.x - origOffsetMax.x);
			float clampY = Mathf.Abs(origOffsetMin.y - origOffsetMax.y);
			
			delta = delta * Time.deltaTime * speed;

			minOffset += delta;
			maxOffset += delta;

			minOffset.x = Mathf.Clamp(minOffset.x, -clampX, 0);
			minOffset.y = Mathf.Clamp(minOffset.y, -clampY, 0);
			maxOffset.x = Mathf.Clamp(maxOffset.x, 0, clampX);
			maxOffset.y = Mathf.Clamp(maxOffset.y, 0, clampY);

			rectTransform.offsetMin = minOffset;
			rectTransform.offsetMax = maxOffset;
		}
		
		public void SetSprite(Sprite sprite)
		{
			Image image = GetComponent<Image>();
			if (image != null)
			{
				image.sprite = sprite;
				gameObject.SetActive(sprite != null);
			}
		}

		#endregion
	}
}