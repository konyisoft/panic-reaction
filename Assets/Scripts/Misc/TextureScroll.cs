using UnityEngine;

namespace Konyisoft.Panic
{
	public class TextureScroll : GameBehaviour
	{
		public Vector2 speed = Vector2.one;

		Vector2 offset = Vector2.zero;
		Renderer _renderer;

		void Awake()
		{
			_renderer = GetComponent<Renderer>();
		}

		void Update()
		{
			if (_renderer != null && _renderer.material != null)
			{
				offset += speed * Time.deltaTime;
				_renderer.material.SetTextureOffset ("_MainTex", offset);
			}
		}
	}
}