﻿using System;
using UnityEngine;

namespace Konyisoft.Panic
{
	public class GameManager : Singleton<GameManager>
	{
		#region Fields

		public Timer timer;

		int score;
		int lives;
		int health;

		#endregion

		#region Unity callbacks

		void Awake()
		{
			if (timer == null)
			{
				Debug.LogError("Timer not attached to GameManager.");
				return;
			}

			LevelManager.Instance.onLevelLoaded += OnLevelLoaded;
			LevelManager.Instance.onLevelUnloaded += OnLevelUnloaded;
		}

		void Start()
		{
			// Timer delegates
			timer.onTimeSet += OnTimeSet;
			timer.onTimeTick += OnTimeTick;
			timer.onTimeElapsed += OnTimeElapsed;
			
			StartGame();
		}
		
		void Update()
		{
			// Quit app
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				Application.Quit();
			}

#if UNITY_EDITOR
			// Screenshot with UI
			if (Input.GetKeyUp(KeyCode.C) && LevelManager.Instance.HasLevelLoaded)
			{
				string fileName = "Screenshot {0}.png";
				ScreenCapture.CaptureScreenshot(string.Format(fileName, DateTime.Now.ToString()), 0);
			}
#endif			
		}

		#endregion
		
		#region Public methods

		public void ResetValues()
		{
			score = Constants.GameStartValues.Score;
			health = Constants.GameStartValues.Health;
			lives = Constants.GameStartValues.Lives;
			UpdateUI();
		}		
		
		public void AddScore(int n)
		{
			score += n;
			UpdateUI();
		}

		public void DecreaseHealth(int n)
		{
			health -= n;
			UpdateUI();
			
			if (health <= 0)
			{
				Die();
			}
		}
		
		public void SetHealth(int n)
		{
			health = n;
			UpdateUI();
		}
		
		public void DecreaseLives(int n)
		{
			lives -= n;
			UpdateUI();
		}
		
		public void UpdateUI()
		{
			UIManager.Instance.SetScore(score);
			UIManager.Instance.SetHealthBar(health);
		}

		#endregion
		
		#region Private methods
		
		void StartGame()
		{
			Player.Instance.Deactivate();
			ResetValues();					
			LoadLevel(0);
		}
		
		void LoadLevel(int index)
		{
			LevelManager.Instance.LoadLevel(0);
		}

		void UnloadLevel()
		{
			LevelManager.Instance.UnloadLevel();
		}
		
		void Die()
		{
			Player.Instance.SetState(PlayerState.Dead);
			Debug.Log("You are dead");
		}

		void TimeIsUp()
		{
			Debug.Log("Time is up");
		}
		
		#endregion
		
		#region Event handlers
		
		void OnTimeSet()
		{
			UIManager.Instance.SetTime(timer.Time);
		}

		void OnTimeTick()
		{
			UIManager.Instance.SetTime(timer.Time);
		}

		void OnTimeElapsed()
		{
			UIManager.Instance.SetTime(timer.Time);
			TimeIsUp();
		}

		void OnLevelLoaded()
		{
			if (LevelManager.Instance.HasLevelLoaded)
			{
				//Setup timer
				timer.Set(LevelManager.Instance.Level.time);
				timer.Run();

				// Let the main light rotate with the player
 				Level level = LevelManager.Instance.Level;
				level.SetLightParent(Player.Instance.PlayerRig.transform);

				// Set background sprite
				UIManager.Instance.uiBackground.SetSprite(level.background);
				
				// Positioning player to start point
				Vector3 startPosition = level.startPoint != null ? level.startPoint.position : Vector3.zero;
				Player.Instance.transform.position = startPosition;
				Vector3 cameraPosition = startPosition;
				cameraPosition.z = DisplayManager.Instance.playerCamera.transform.position.z;
				DisplayManager.Instance.playerCamera.transform.position = cameraPosition;

				// Activate and start playing
				Player.Instance.Activate();
				Player.Instance.SetState(PlayerState.Playing);
			}
		}

		void OnLevelUnloaded()
		{
			// TODO: implement later
		}

		#endregion
	}
}
