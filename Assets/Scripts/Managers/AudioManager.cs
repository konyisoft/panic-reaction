using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Konyisoft.Panic
{
    public class AudioManager : Singleton<AudioManager>
    {
        #region Classes

        public class AudioData
        {
            public AudioSource audioSource;
            public float origVolume = 1f;
        }

        #endregion

        #region Fields

        public GameObject audioParent;
        public bool preload = true;
        public bool mute = false;

        Dictionary<string, AudioData> cache = new Dictionary<string, AudioData>();

        #endregion

        #region Mono methods

        void Awake()
        {
            if (audioParent == null)
            {
                audioParent = gameObject;
            }

            if (preload)
            {
                Preload();
            }
        }

        void OnDestroy()
        {
            StopAllCoroutines();
        }

        #endregion

        #region Public methods

        public void Preload()
        {
            if (audioParent != null)
            {
                List<AudioSource> audioSources = audioParent.GetComponentsInChildren<AudioSource>().ToList();
                audioSources.ForEach(audioSource =>
                {
                    AudioData data = new AudioData()
                    {
                        audioSource = audioSource,
                        origVolume = audioSource.volume
                    };
                    AddToCache(audioSource.gameObject.name, data);
                });
            }
        }

        public void PlaySound(AudioData data, float fadeDuration)
        {
            if (Options.Audio == Options.AudioSetting.MusicAndSound ||
                Options.Audio == Options.AudioSetting.SoundOnly)
            {
                PlayInternal(data, fadeDuration);
            }
        }

        public void PlaySound(AudioData data)
        {
            PlaySound(data, 0);
        }

        public void PlaySound(string name, float fadeDuration)
        {
            PlaySound(GetAudioData(name), fadeDuration);
        }

        public void PlaySound(string name)
        {
            PlaySound(name, 0);
        }

        public void PlayMusic(AudioData data, float fadeDuration)
        {
            if (Options.Audio == Options.AudioSetting.MusicAndSound ||
                Options.Audio == Options.AudioSetting.MusicOnly)
            {
                PlayInternal(data, fadeDuration);
            }
        }

        public void PlayMusic(AudioData data)
        {
            PlayMusic(data, 0);
        }

        public void PlayMusic(string name, float fadeDuration)
        {
            PlayMusic(GetAudioData(name), fadeDuration);
        }

        public void PlayMusic(string name)
        {
            PlayMusic(name, 0);
        }

        public void Stop(AudioData data, float fadeDuration)
        {
            if (!mute && data != null && data.audioSource != null)
            {
                if (fadeDuration > 0)
                {
                    StopAllCoroutines();
                    StartCoroutine(CO_StopWithFade(data, fadeDuration));
                }
                else
                {
                    data.audioSource.Stop();
                }
            }
        }

        public void Stop(string name, float fadeDuration)
        {
            Stop(GetAudioData(name), fadeDuration);
        }

        public void Stop(string name)
        {
            Stop(name, 0);
        }

        public bool IsPlaying(AudioData data)
        {
            return data != null && data.audioSource != null && data.audioSource.isPlaying;
        }

        public bool IsPlaying(string name)
        {
            return IsPlaying(GetAudioData(name));
        }

        public float GetTime(AudioData data)
        {
            return  data != null && data.audioSource != null  ? data.audioSource.time : 0f;
        }

        public float GetTime(string name)
        {
            return GetTime(GetAudioData(name));
        }

        public AudioData GetAudioData(string name)
        {
            // Try to get from cache
            if (cache.ContainsKey(name))
            {
                return cache[name];
            }
            // Otherwise try to find it
            else if (audioParent != null)
            {
                GameObject go = GameObject.Find(audioParent.name + "/" + name);
                if (go != null)
                {
                    AudioSource audioSource = go.GetComponent<AudioSource>();
                    AudioData data = new AudioData()
                    {
                        audioSource = audioSource,
                        origVolume = audioSource.volume
                    };
                    AddToCache(name, data);  // Add to cache
                    return data;
                }
            }
            return null;
        }

        #endregion

        #region Private methods

        void PlayInternal(AudioData data, float fadeDuration)
        {
            if (!mute && data != null && data.audioSource != null)
            {
                if (data.audioSource.isPlaying)
                    data.audioSource.Stop();

                if (fadeDuration > 0)
                {
                    data.audioSource.volume = 0;
                    StartCoroutine(CO_Fade(data, 0, data.origVolume, fadeDuration));
                }

                data.audioSource.Play();
            }
        }


        void AddToCache(string key, AudioData data)
        {
            if (!cache.ContainsKey(key))
                cache.Add(key, data);
        }

        IEnumerator CO_StopWithFade(AudioData data, float fadeDuration)
        {
            if (data != null && data.audioSource != null)
            {
                yield return StartCoroutine(CO_Fade(data, data.audioSource.volume, 0, fadeDuration));
                data.audioSource.Stop();
                data.audioSource.volume = data.origVolume; // Reset volume after stop
            }
            yield return null;
        }

        IEnumerator CO_Fade(AudioData data, float fromVolume, float toVolume, float duration)
        {
            if (data != null && data.audioSource != null)
            {
                float startTime = Time.time;
                while (Time.time - startTime < duration)
                {
                    data.audioSource.volume = Mathf.Lerp(fromVolume, toVolume, (Time.time - startTime) / duration);
                    yield return null;
                }
                data.audioSource.volume = toVolume;
            }
            yield return null;
        }

        #endregion

    }
}