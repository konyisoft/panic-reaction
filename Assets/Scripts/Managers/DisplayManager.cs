using UnityEngine;

namespace Konyisoft.Panic
{
	public class DisplayManager : Singleton<DisplayManager>
	{
		#region Enumerations

		public enum ShakeMode
		{
			Increasing,
			Decreasing
		}

		#endregion

		#region Fields
		
		public Camera playerCamera;
		public Camera uiCamera;
		public float shakeIntensity = 0.035f;
		public float shakeDecay = 0.075f;
		public ShakeMode shakeMode = ShakeMode.Decreasing;

		float currentIntensity = 0.05f;
		bool isShaking = false;

		#endregion
		
		#region Mono methods

		void Awake()
		{
			Screen.sleepTimeout = SleepTimeout.NeverSleep;

			if (playerCamera == null)
			{
				Debug.LogError("No player camera attached.");
				return;
			}
			
			if (uiCamera == null)
			{
				Debug.LogError("No UI camera attached.");
				return;
			}
		}

		void LateUpdate()
		{
			if (isShaking)
			{
				// Shaking
				if (currentIntensity >= 0f)
				{
					// Calculate position, rotation and intensity
					Vector3 shakePosition = Random.insideUnitSphere * currentIntensity;
					shakePosition.z = 0;
					playerCamera.transform.localPosition = playerCamera.transform.localPosition + shakePosition;
					
					Quaternion shakeRotation = new Quaternion(
						playerCamera.transform.localRotation.x + Random.Range(-currentIntensity, currentIntensity) * 0.02f,
						playerCamera.transform.localRotation.y + Random.Range(-currentIntensity, currentIntensity) * 0.02f,
						playerCamera.transform.localRotation.z + Random.Range(-currentIntensity, currentIntensity) * 0.02f,
						playerCamera.transform.localRotation.w + Random.Range(-currentIntensity, currentIntensity) * 0.02f
					);
					playerCamera.transform.localRotation = shakeRotation;
					currentIntensity -= shakeDecay * Time.deltaTime * (shakeMode == ShakeMode.Decreasing ? 1f : -1f);
				}
		
				// Stop shaking
				if ((shakeMode == ShakeMode.Decreasing && currentIntensity <= 0f) ||
					(shakeMode == ShakeMode.Increasing && currentIntensity >= shakeIntensity))
				{
					isShaking = false;
				}		
			}
		}		
		
		#endregion

		#region Public methods

		public void SetPlayerCameraPosition(Vector3 position)
		{
			playerCamera.transform.position = position;
		}

		public void SetPlayerCameraRotation(Quaternion rotation)
		{
			playerCamera.transform.rotation = rotation;
		}

		public void ShakePlayerCamera()
		{
			currentIntensity = shakeMode == ShakeMode.Decreasing ? shakeIntensity : 0f;
			isShaking = true;
		}

		#endregion
	}
}
