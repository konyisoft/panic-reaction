using UnityEngine;

namespace Konyisoft.Panic
{
	public class Timer : GameBehaviour
	{
		#region Enumerations

		public enum Direction
		{
			Down = -1,
			Up = 1
		}

		#endregion

		#region Fields

		public Direction direction = Direction.Up;
		public int maxTime = 60;
		public float slice = 1.0f;
		public bool runAtStart = false;

		public delegate void OnTimeSet();
		public delegate void OnTimeTick();
		public delegate void OnTimeElapsed();

		public event OnTimeSet onTimeSet;
		public event OnTimeTick onTimeTick;
		public event OnTimeElapsed onTimeElapsed;

		bool isRunning;
		float timer;
		int time;

		#endregion

		#region Properties

		public int Time
		{
			get { return time; }
		}

		public int ElapsedTime
		{
			get { return direction == Direction.Down ? maxTime - time : time; }
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			Set();
		}

		void Start()
		{
			if (runAtStart)
			{
				Run();
			}
		}

		void Update()
		{
			if (isRunning)
			{
				timer += UnityEngine.Time.deltaTime;
				if (timer >= slice)
				{
					time += 1 * (int)direction;
					timer -= slice; // Timer restarts with the remaining value
					if (onTimeTick != null) // Call delegate
					{
						onTimeTick();
					}
				}

				if ((direction == Direction.Up && time >= maxTime) || (direction == Direction.Down && time <= 0))
				{
					isRunning = false;
					if (onTimeElapsed != null) // Call delegate
					{
						onTimeElapsed();
					}
				}
			}
		}

		#endregion

		#region Public methods

		public void Set(int maxTime = 0)
		{
			if (maxTime > 0)
			{
				this.maxTime = maxTime;
			}
			timer = 0;
			time = direction == Direction.Down ? this.maxTime : 0;
			if (onTimeSet != null) // Call delegate
			{
				onTimeSet();
			}
		}

		public void Run()
		{
			isRunning = true;
		}

		public void Stop()
		{
			isRunning = false;
			Set();
		}

		public void Pause()
		{
			isRunning = false;
		}

		public void Resume()
		{
			isRunning = true;
		}

		public static string GetTimeString(int time)
		{
			int minutes = Mathf.FloorToInt(time / 60);
			int seconds = Mathf.FloorToInt(time - minutes * 60);
			return string.Format("{0:00}:{1:00}", minutes, seconds);
		}

		#endregion
	}
}
