﻿namespace Konyisoft.Panic
{
    public static class Options
    {
        #region Enumerations
        public enum MobileInputMode
        {
            Touch,
            Buttons
        }

        public enum AudioSetting
        {
            MusicAndSound = 0,
            MusicOnly = 1,
            SoundOnly = 2,
            None = 3,
        }

		#endregion	
		
		public static AudioSetting Audio = AudioSetting.MusicAndSound;

        public static MobileInputMode MobileInput = MobileInputMode.Buttons;
    }
}


