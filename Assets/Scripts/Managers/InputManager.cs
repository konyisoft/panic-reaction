using UnityEngine;
using UnityEngine.EventSystems;

namespace Konyisoft.Panic
{
	public class InputManager : Singleton<InputManager>
	{
		#region Public methods

		public bool Raycast(Vector2 position, GameObject go, int layerMask)
		{
			bool r = false;
			Collider coll = go.GetComponent<Collider>();
			if (coll != null)
			{
				Ray ray = Camera.main.ScreenPointToRay(position);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask) && coll == hit.collider)
				{
					r = true;
				}
			}
			return r;
		}		
		
		public bool IsMouseOverUI()
		{
			return EventSystem.current.IsPointerOverGameObject();
		}

		public bool IsTouchOverUI(int fingerId)
		{
			return EventSystem.current.IsPointerOverGameObject(fingerId);
		}
		
		public bool IsTouchOverAnyUI()
		{
			bool r = false;
			for (int i = 0; i < Input.touchCount; i++)
			{
				if (EventSystem.current.IsPointerOverGameObject(Input.touches[i].fingerId))
				{
					r = true;
					break;
				}
			}
			return r;
		}
		
		#endregion
		
		#region Touch handling

		public Touch? GetTouchByFingerId(int fingerId)
		{
			Touch? touch = null;
			for (int i = 0; i < Input.touchCount; i++)
			{
				if (fingerId == Input.touches[i].fingerId)
				{
					touch = Input.touches[i];
					break;
				}
			}
			return touch;
		}

		public bool HasBeganTouch()
		{
			bool r = false;
			for (int i = 0; i < Input.touchCount; i++)
			{
				if (Input.touches[i].phase == TouchPhase.Began)
				{
					r = true;
					break;
				}
			}
			return r;
		}

		public Touch? GetLastBeganTouch()
		{
			Touch? touch = null;
			for (int i = 0; i < Input.touchCount; i++)
			{
				Touch t = Input.touches[i];
				if (t.phase == TouchPhase.Began)
				{
					touch = t;
					break;
				}
			}
			return touch;
		} 		
		
		#endregion
	}
}
