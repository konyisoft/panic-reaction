﻿using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Panic
{
	public class UIManager : Singleton<UIManager>
	{
		#region Fields

		public UIBackground uiBackground;
		public Text timeText;
		public Text scoreText;
		public Image healthBar;

		#endregion

		#region Mono methods

		void Awake() 
		{
			if (uiBackground == null)
			{
				Debug.LogError("No UIBackground attched", this);
				return;
			}
		}

		#endregion

		#region Public methods

		public void SetTime(int time)
		{
			if (timeText != null)
			{
				int minutes = Mathf.FloorToInt(time / 60);
				int seconds = Mathf.FloorToInt(time - minutes * 60);
				timeText.text = string.Format(Constants.Texts.Timer, minutes, seconds);
			}
		}

		public void SetScore(int score)
		{
			if (scoreText != null)
			{
				scoreText.text = string.Format(Constants.Texts.Score, score);
			}
		}
		
		public void SetHealthBar(int health)
		{
			if (healthBar != null)
			{
				float normalizedHealth = MathUtils.Normalize(health, 0, Constants.GameStartValues.Health);
				healthBar.fillAmount = normalizedHealth;
			}
		}
		
		#endregion
	}
}
