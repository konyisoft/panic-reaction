﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Linq;

namespace Konyisoft.Panic
{
	public class LevelManager : Singleton<LevelManager>
	{
		#region Enumerations

		enum LoadState
		{
			None,
			Loading,
			Unloading
		}

		#endregion
		
		#region Fields

		public int levelCount;

		Level level;
		Scene scene;
		LoadState loadState = LoadState.None;

		#endregion

		#region Delegates

		public delegate void OnLevelEvent();
		public OnLevelEvent onLevelLoaded;
		public OnLevelEvent onLevelUnloaded;

		#endregion

		#region Properties

		public bool HasLevelLoaded
		{
			get { return level != null; }
		}

		public Level Level
		{
			get { return level; }
		}

		public Scene Scene
		{
			get { return scene; }
		}

		#endregion

		#region Mono methods

		void  Awake()
		{
			SceneManager.sceneLoaded += OnSceneLoaded;
			SceneManager.sceneUnloaded += OnSceneUnloaded;
		}

        #endregion

        #region Public methods

        public void LoadLevel(int levelIndex)
		{
			if (!HasLevelLoaded)
			{
				string sceneName = levelIndex.ToString("D3");
				loadState = LoadState.Loading;
				StartCoroutine(CO_LoadScene(sceneName, LoadSceneMode.Additive));
			}
		}

		public void UnloadLevel()
		{
			if (HasLevelLoaded)
			{
				// Put main light back to the scene (detach from player)
				level.ResetLightParent();
				loadState = LoadState.Unloading;
				StartCoroutine(CO_UnloadScene(scene));
			}
		}

		#endregion

		#region Private methods

		IEnumerator CO_LoadScene(string sceneName, LoadSceneMode mode)
		{
			AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
			yield return asyncOperation;
		}

		IEnumerator CO_UnloadScene(Scene scene)
		{
			AsyncOperation asyncOperation = SceneManager.UnloadSceneAsync(scene);
			yield return asyncOperation;
		}

		#endregion

		#region Event handlers

		void OnSceneLoaded(Scene scene, LoadSceneMode mode)
		{
			if (loadState != LoadState.Loading)
				return;
			
			// Trying to find the parent object
			GameObject levelObject = scene.GetRootGameObjects()
				.ToList()
				.Find(rootObject => rootObject.name == Constants.ObjectNames.Level);

			if (levelObject != null)
			{
				Level level = levelObject.GetComponent<Level>();
				if (level != null)
				{
					this.level = level;
					this.scene = scene;
					// Call event handlers
					if (onLevelLoaded != null)
					{
						onLevelLoaded();
					}
				}
				else
				{
					Debug.LogError("Level component not attached to root object on scene '" + scene.name + "'");
				}
			}
			else
			{
				Debug.LogError("Level object not found on scene '" + scene.name + "'");
			}
			loadState = LoadState.None;
        }

		void OnSceneUnloaded(Scene scene)
        {
            if (loadState != LoadState.Unloading)
				return;

			// Call event handlers
			if (onLevelUnloaded != null)
			{
				onLevelUnloaded();
			}
			loadState = LoadState.None;
        }		
		
		#endregion
	}
}
