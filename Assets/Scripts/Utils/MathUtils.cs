﻿using System;
using UnityEngine;

namespace Konyisoft.Panic
{
	public static class MathUtils
	{
		public static float Normalize(float value, float min, float max)
		{
			return (value - min) / (max - min);
		}
	}
}
