using UnityEngine;

namespace Konyisoft.Panic
{
    public static class Extensions
	{
		#region Color32

		public static bool Compare(this Color32 color, Color32 other)
		{
			// No alpha checked
			return color.r == other.r && color.g == other.g && color.b == other.b;
		}

		public static bool CompareWithAlpha(this Color32 color, Color32 other)
		{
			return color.r == other.r && color.g == other.g && color.b == other.b && color.a == other.a;
		}

		#endregion

		#region String

		public static string RemoveCloneSuffix(this string s)
		{
			return s.Replace("(Clone)", "");
		}

		#endregion
	}
}
