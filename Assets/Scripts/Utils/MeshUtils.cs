﻿using System;
using UnityEngine;

namespace Konyisoft.Panic
{
    public static class MeshUtils
    {
        public static Mesh CombineMeshes(Transform transform)
        {
            Mesh mesh = null;
            if (transform != null)
            {
                // Get meshfilters
                MeshFilter[] meshFilters = transform.GetComponentsInChildren<MeshFilter>();

                if (meshFilters.Length > 0)
                {
                    CombineInstance[] combine = new CombineInstance[meshFilters.Length];
                    int i = 0;
                    while (i < meshFilters.Length)
                    {
                        combine[i].mesh = meshFilters[i].sharedMesh;
                        combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
                        i++;
                    }
                    // Combine meshes
                    mesh = new Mesh();
                    mesh.CombineMeshes(combine);
                    mesh.Weld(1);
                }
            }
            return mesh;
        }
    }

}
