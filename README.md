# Panic Reaction
**by Konyisoft**

An arcade game where the player rotates a maze to guide a character towards a goal.  
Created with Unity3D 2017.4 on Ubuntu.  
Example/prototype game.

Try it out in browser: [WebGL build](http://konyisoft.gitlab.io/panic-reaction/)

![Screenshot](Misc/screenshot.jpg)
